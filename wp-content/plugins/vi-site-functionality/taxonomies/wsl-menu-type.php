<?php

function wsl_menu_type_init() {
	register_taxonomy( 'wsl-menu-type', array( 'wsl-menu' ), array(
		'hierarchical'      => false,
		'public'            => true,
		'show_in_nav_menus' => false,
		'show_ui'           => false,
		'show_admin_column' => false,
		'query_var'         => true,
		'rewrite'           => true,
		'capabilities'      => array(
			'manage_terms'  => 'edit_posts',
			'edit_terms'    => 'edit_posts',
			'delete_terms'  => 'edit_posts',
			'assign_terms'  => 'edit_posts'
		),
		'labels'            => array(
			'name'                       => __( 'Menu Group Types', 'vi-site-functionality' ),
			'singular_name'              => _x( 'Menu Group Type', 'taxonomy general name', 'vi-site-functionality' ),
			'search_items'               => __( 'Search Menu Group Types', 'vi-site-functionality' ),
			'popular_items'              => __( 'Popular Menu Group Types', 'vi-site-functionality' ),
			'all_items'                  => __( 'All Menu Group Types', 'vi-site-functionality' ),
			'parent_item'                => __( 'Parent Menu Group Type', 'vi-site-functionality' ),
			'parent_item_colon'          => __( 'Parent Menu Group Type:', 'vi-site-functionality' ),
			'edit_item'                  => __( 'Edit Menu Group Type', 'vi-site-functionality' ),
			'update_item'                => __( 'Update Menu Group Type', 'vi-site-functionality' ),
			'add_new_item'               => __( 'New Menu Group Type', 'vi-site-functionality' ),
			'new_item_name'              => __( 'New Menu Group Type', 'vi-site-functionality' ),
			'separate_items_with_commas' => __( 'Separate Menu Group Types with commas', 'vi-site-functionality' ),
			'add_or_remove_items'        => __( 'Add or remove Menu Group Types', 'vi-site-functionality' ),
			'choose_from_most_used'      => __( 'Choose from the most used Menu Group Types', 'vi-site-functionality' ),
			'not_found'                  => __( 'No Menu Group Types found.', 'vi-site-functionality' ),
			'menu_name'                  => __( 'Menu Group Types', 'vi-site-functionality' ),
		),
		'show_in_rest'      => true,
		'rest_base'         => 'wsl-menu-type',
		'rest_controller_class' => 'WP_REST_Terms_Controller',
	) );

}
add_action( 'init', 'wsl_menu_type_init' );
