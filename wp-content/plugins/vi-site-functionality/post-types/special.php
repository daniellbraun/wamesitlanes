<?php

function special_init() {
	register_post_type( 'specials', array(
		'labels'            => array(
			'name'                => __( 'Specials', 'vi-site-functionality' ),
			'singular_name'       => __( 'Special', 'vi-site-functionality' ),
			'all_items'           => __( 'All Specials', 'vi-site-functionality' ),
			'new_item'            => __( 'New Special', 'vi-site-functionality' ),
			'add_new'             => __( 'Add New', 'vi-site-functionality' ),
			'add_new_item'        => __( 'Add New Special', 'vi-site-functionality' ),
			'edit_item'           => __( 'Edit Special', 'vi-site-functionality' ),
			'view_item'           => __( 'View Special', 'vi-site-functionality' ),
			'search_items'        => __( 'Search Specials', 'vi-site-functionality' ),
			'not_found'           => __( 'No Specials found', 'vi-site-functionality' ),
			'not_found_in_trash'  => __( 'No Specials found in trash', 'vi-site-functionality' ),
			'parent_item_colon'   => __( 'Parent Special', 'vi-site-functionality' ),
			'menu_name'           => __( 'Specials', 'vi-site-functionality' ),
		),
		'public'            => true,
		'hierarchical'      => true,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'revisions' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'special',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'special_init', 1 );

function special_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['special'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Special updated. <a target="_blank" href="%s">View Special</a>', 'vi-site-functionality'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'vi-site-functionality'),
		3 => __('Custom field deleted.', 'vi-site-functionality'),
		4 => __('Special updated.', 'vi-site-functionality'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Special restored to revision from %s', 'vi-site-functionality'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Special published. <a href="%s">View Special</a>', 'vi-site-functionality'), esc_url( $permalink ) ),
		7 => __('Special saved.', 'vi-site-functionality'),
		8 => sprintf( __('Special submitted. <a target="_blank" href="%s">Preview Special</a>', 'vi-site-functionality'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Special scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Special</a>', 'vi-site-functionality'),
		// translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Special draft updated. <a target="_blank" href="%s">Preview Special</a>', 'vi-site-functionality'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'special_updated_messages' );
