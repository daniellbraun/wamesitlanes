<?php

function wsl_menu_init() {
	register_post_type( 'wsl-menu', array(
		'labels'            => array(
			'name'                => __( 'Menu Groups', 'vi-site-functionality' ),
			'singular_name'       => __( 'Menu Group', 'vi-site-functionality' ),
			'all_items'           => __( 'All Menu Groups', 'vi-site-functionality' ),
			'new_item'            => __( 'New Menu Group', 'vi-site-functionality' ),
			'add_new'             => __( 'Add New Menu Group', 'vi-site-functionality' ),
			'add_new_item'        => __( 'Add New Menu Group', 'vi-site-functionality' ),
			'edit_item'           => __( 'Edit Menu Group', 'vi-site-functionality' ),
			'view_item'           => __( 'View Menu Group', 'vi-site-functionality' ),
			'search_items'        => __( 'Search Menu Groups', 'vi-site-functionality' ),
			'not_found'           => __( 'No Menu Groups found', 'vi-site-functionality' ),
			'not_found_in_trash'  => __( 'No Menu Groups found in trash', 'vi-site-functionality' ),
			'parent_item_colon'   => __( 'Parent Menu Group', 'vi-site-functionality' ),
			'menu_name'           => __( 'Menus', 'vi-site-functionality' ),
		),
		'public'            => true,
		'hierarchical'      => false,
		'show_ui'           => true,
		'show_in_nav_menus' => true,
		'supports'          => array( 'title', 'editor', 'revisions' ),
		'has_archive'       => true,
		'rewrite'           => true,
		'query_var'         => true,
		'menu_icon'         => 'dashicons-admin-post',
		'show_in_rest'      => true,
		'rest_base'         => 'wsl-menu',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'wsl_menu_init' );

function wsl_menu_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['wsl-menu'] = array(
		0 => '', // Unused. Messages start at index 1.
		1 => sprintf( __('Menu Group updated. <a target="_blank" href="%s">View Menu</a>', 'vi-site-functionality'), esc_url( $permalink ) ),
		2 => __('Custom field updated.', 'vi-site-functionality'),
		3 => __('Custom field deleted.', 'vi-site-functionality'),
		4 => __('Menu Group updated.', 'vi-site-functionality'),
		/* translators: %s: date and time of the revision */
		5 => isset($_GET['revision']) ? sprintf( __('Menu Group restored to revision from %s', 'vi-site-functionality'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		6 => sprintf( __('Menu Group published. <a href="%s">View Menu Group</a>', 'vi-site-functionality'), esc_url( $permalink ) ),
		7 => __('Menu Group saved.', 'vi-site-functionality'),
		8 => sprintf( __('Menu Group submitted. <a target="_blank" href="%s">Preview Menu Group</a>', 'vi-site-functionality'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		9 => sprintf( __('Menu Group scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Menu Group</a>', 'vi-site-functionality'),
		// translators: Publish box date format, see https://secure.php.net/manual/en/function.date.php
		date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		10 => sprintf( __('Menu Group draft updated. <a target="_blank" href="%s">Preview Menu Group</a>', 'vi-site-functionality'), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'wsl_menu_updated_messages' );
