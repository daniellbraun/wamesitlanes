<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin and
 * registers the activation and deactivation functions.
 *
 * @wordpress-plugin
 * Plugin Name:       Wamesit Lanes Site Functionality
 * Plugin URI:        https://www.vectorandink.com/
 * Description:       This plugin contains all persistent functionality for the Wamesit Lanes site.
 * Version:           0.5
 * Author:            Vector & Ink
 * Author URI:        https://www.vectorandink.com/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Load required dependencies for this plugin.
 */
$includes = [
	'includes/acf.php',                  // ACF functions.
	'includes/admin-customizations.php', // Admin customizations.
	'includes/archive-pages.php',        // Select custom archive page for each CPT.
	'includes/rewrite-rules.php',        // Custom rewrite rules.
	'post-types/special.php',        // Specials CPT.
	'includes/ajax.php',        // AJAX functions.
	'post-types/wsl-menu.php', // Menu Group CPT
	'taxonomies/wsl-menu-type.php', // Menu Group Type tax
	'includes/timber.php', // Timber event stuff
	'includes/Event.php', // Event Class
];

foreach ( $includes as $file ) {
	if ( ! $filepath = plugin_dir_path( __FILE__ ) ) {
		trigger_error( sprintf( 'Error locating %s for inclusion', $file ), E_USER_ERROR );
	}
	require_once plugin_dir_path( __FILE__ ) . $file;
}
unset( $file );
