<?php

namespace VI\Ajax;

use Timber;
use VI;

add_action( 'wp_ajax_nopriv_events', __NAMESPACE__ . '\\get_events_by_cat' );
add_action( 'wp_ajax_events', __NAMESPACE__ . '\\get_events_by_cat' );

/**
 * Get events by category.
 */
function get_events_by_cat() {
	$results = [];

	$category_link_part = '';
	$category_name = '';
	$category_desc = '';

	$args = array(
		'post_type'         => 'event',
		'event_start_after' => 'today',
	);

	// Add terms if exist.
	if ( 'all' !== $_GET['category'] ) {

		$category_link_part = 'category/' . $_GET['category'] . '/';

		$term = new Timber\Term($_GET['category']);
		$category_name = $term->name;
		$category_desc = $term->description;
		$category_slug = $term->slug;

		$args['tax_query'] = array(
			array(
				'taxonomy' => 'event-category',
				'field'    => 'slug',
				'terms'    => $_GET['category'],
			),
		);
	} else {

		$post = new Timber\Post( 11 ); // Get the corresponding archive page.
		$category_desc = $post->content;
		$category_name = $post->sub_title;
	}

	$posts = new Timber\PostQuery( $args );


	$pagination = $posts->pagination();
	$pages = &$pagination->pages;

	foreach ( $pages as &$page ) {

		if ( isset($page['link']) ) {

			$page['link'] = '/events/' . $category_link_part . 'page/' . $page['name'] . '/';

		}
	}

	$results['content'] = Timber\Timber::compile( 'partials/events-list.twig', array( 'events' => $posts ) );
	$results['category'] = array(
		'name' => $category_name,
		'desc' => $category_desc,
		'slug' => $category_slug,
	);

	wp_send_json( $results );
}