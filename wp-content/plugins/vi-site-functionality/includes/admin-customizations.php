<?php
/**
 * Register custom post types.
 *
 * @since      0.5.0
 * @package    VI_Site_Functionality
 * @subpackage VI_Site_Functionality/includes
 */

namespace VectorAndInk;

// Enqueue admin scripts/styles
add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\\enqueue_admin_scripts' );
add_action( 'login_enqueue_scripts', __NAMESPACE__ . '\\enqueue_login_style' );

// Login page logo link & title.
add_filter( 'login_headerurl', __NAMESPACE__ . '\\login_headerurl' );
add_filter( 'login_headertitle', __NAMESPACE__ . '\\login_headertitle' );

// Change Yoast meta box priority, so it gets moved to bottom of all admin pages.
add_filter( 'wpseo_metabox_prio', function () {
	return 'low';
} );

// Disable tabindex for all Gravity Forms for better accessibility.
add_filter( 'gform_tabindex', '__return_false' );

// Add CPT admin columns.
add_action( 'plugins_loaded', __NAMESPACE__ . '\\add_admin_columns' );

// Add Eat and Drink page links to Admin Menu under 'Menus'
add_action( 'admin_menu', __NAMESPACE__ . '\\action_add_admin_menu_pages' );

// Filter CPT.
add_action( 'restrict_manage_posts', __NAMESPACE__ . '\\action_add_taxonomy_filter' );

// Remove unwanted menu items from Admin Menu
add_action( 'admin_menu', __NAMESPACE__ . '\\remove_menus' );

// Allow only "Eat" or "Drink" menu groups as appropriate to page to appear in Menu Layout -> Menu Group select lists
add_filter( 'acf/fields/post_object/query/key=field_56fa98efe9b92', __NAMESPACE__ . '\\acf_filter_menu_posts', 10, 3 ); // Single Group
add_filter( 'acf/fields/post_object/query/key=field_56fa992ae9b96', __NAMESPACE__ . '\\acf_filter_menu_posts', 10, 3 ); // Double Group Col. 1
add_filter( 'acf/fields/post_object/query/key=field_56fa993fe9b97', __NAMESPACE__ . '\\acf_filter_menu_posts', 10, 3 ); // Double Group Col. 2
/**
 * On the Eat and Drink pages filter the posts that show up in the 'menu group'
 * select field to contain only menu groups with the 'Eat' tag on the eat page,
 * and only menu groups with the 'Drink' tag on the drink page.
 *
 * @param $args
 * @param $field
 * @param $post_id
 *
 * @return mixed
 */
function acf_filter_menu_posts( $args, $field, $post_id ) {

	$post = get_post( $post_id );
	$post_slug = $post->post_name;

	if ( $post_slug == 'eat' || $post_slug == 'drink') {
		$args['tax_query'] = array(
			array(
				'taxonomy' => 'wsl-menu-type',
				'field'    => 'slug',
				'terms'    => $post_slug,
			),
		);
		return $args;
	}
	return $args;
}

/**
 * Changes the logo link on the login page.
 *
 * @return string
 */
function login_headerurl() {
	return home_url();
}

/**
 * Changes the title for the logo on the login page.
 *
 * @return string
 */
function login_headertitle() {
	return get_bloginfo( 'name' );
}

/**
 * Enqueue scripts/styles for the admin.
 */
function enqueue_admin_scripts() {
	wp_enqueue_style( 'font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', '', '4.5.0', 'all' );
	wp_enqueue_style( 'admin-style', plugin_dir_url( dirname( __FILE__ ) ) . 'assets/css/vi-admin.css', '', '1.0.0', 'all' );
	wp_enqueue_script( 'vi-admin-script', plugin_dir_url( dirname( __FILE__ ) ) . 'assets/js/vi-admin.js', false, '1.0.0' );
}

/**
 * Enqueue style for login page.
 */
function enqueue_login_style() {
	wp_enqueue_style( 'login-style', plugin_dir_url( dirname( __FILE__ ) ) . 'assets/css/vi-admin-login.css', '', '1.0.0', 'all' );
}

/**
 * Add columns to admin pages.
 */
function add_admin_columns() {
	if ( class_exists( 'Jigsaw' ) ) {
		\Jigsaw::add_column( 'wsl-menu', 'Menu Page', function ( $pid ) {
			$data         = array();
			$data['post'] = \Timber\Timber::get_post( $pid );
			\Timber\Timber::render( 'wsl-menu-page.twig', $data );
		}, 2 );
	}
}

/**
 * Add links to the admin menu.
 *
 * Add 'Eat' and 'Drink' links under 'Menus' admin menu.
 */
function action_add_admin_menu_pages() {
	$eat_id = get_posts( array( 'name' => 'eat', 'post_type' => 'page') )[0]->ID;
	$drink_id = get_posts( array( 'name' => 'drink', 'post_type' => 'page') )[0]->ID;
	add_submenu_page(
		'edit.php?post_type=wsl-menu',
		'Eat',
		'Eat',
		'edit_pages',
		'post.php?post=' . $eat_id . '&action=edit'
	);

	add_submenu_page(
		'edit.php?post_type=wsl-menu',
		'Drink',
		'Drink',
		'edit_pages',
		'post.php?post=' . $drink_id . '&action=edit'
	);
}

/**
 * Adds a taxonomy select field to the 'wsl-menu' post type admin
 * page to filter posts by the'wsl-menu-type' taxonomy (eat/drink).
 *
 * @link http://wpquestions.com/question/showChrono/id/7548
 */
function action_add_taxonomy_filter() {
	global $typenow;

	// Taxonomies to display in dropdown.
	$taxonomies = array( 'wsl-menu-type' );

	// Post type to show dropdown on.
	if ( $typenow === ( 'wsl-menu' ) ) {
		foreach ( $taxonomies as $tax_slug ) {
			$tax_obj  = get_taxonomy( $tax_slug );
			$tax_name = $tax_obj->labels->name;
			$terms    = get_terms( $tax_slug );

			if ( count( $terms ) > 0 ) {
				echo '<select name="' . $tax_slug . '" id="' . $tax_slug . '" class="postform">';
				echo '<option value="">All ' . $tax_name . '</option>';

				foreach ( $terms as $term ) {
					echo '<option value=' . $term->slug;

					if ( isset( $_GET[ $tax_slug ] ) ) {
						echo $_GET[ $tax_slug ] == $term->slug ? ' selected="selected"' : '';
					}

					echo '>' . $term->name . ' (' . $term->count . ')</option>';
				}

				echo '</select>';
			}
		}
	}
}

/**
 * Remove unnecessary menu items from admin menu for this site
 */
function remove_menus() {
	remove_menu_page( 'edit.php' ); //Posts
	remove_menu_page( 'edit-comments.php' );
	remove_menu_page( 'tools.php' );
}