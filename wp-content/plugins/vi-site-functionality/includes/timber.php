<?php

namespace AdLit\Timber;

add_filter( 'Timber\PostClassMap', __NAMESPACE__ . '\\timber_classmap', 10 );

/**
 * Set default classes for Timber to use for each custom post type.
 *
 * @return array
 */
function timber_classmap() {
    return array(
        'event'  => 'VI\Event',
    );
}