<?php
/**
 * Class for wis-listing posts.
 *
 * Includes functions for checking for paid features and for getting address
 * and phone number.
 */

namespace VI;

use Timber;
use DateTime;

/**
 * Extends Timber\Post for the event post type.
 *
 * Adds some helpful functions to simplify the twig templates.
 *
 * @package WordPress
 */

/**
 * Class Event
 */
class Event extends Timber\Post {
	/**
	 * @var int Added by EO.
	 */
	public $occurrence_id;

	/**
	 * Set this event as the global post. Event Organiser needs it to be
	 * the global for the templates to render properly.
	 */
	function set_event() {
		global $post;

		if ( $post->post_type === 'event' ) {
			$post = $this;
		}
	}

	function recurs() {
		return eo_recurs( $this->ID );
	}

	/**
	 * Event start date.
	 *
	 * @param string $format Date format.
	 *
	 * @return DateTime|string
	 */
	function start_date( $format = 'U' ) {
		return eo_get_the_start( $format, $this->id, $this->occurrence_id );
	}

	/**
	 * Event end date.
	 *
	 * @param string $format Date format.
	 *
	 * @return DateTime|string
	 */
	function end_date( $format = 'U' ) {
		return eo_get_the_end( $format, $this->id, $this->occurrence_id );
	}

	/**
	 * Format start/end time for events.
	 *
	 * @param string $format Time format.
	 *
	 * @return string
	 */
	function time_span( $format = 'g:i A' ) {
		if ( eo_is_all_day( $this->ID ) ) {
			return 'All Day';
		}

		return $this->start_date( $format ) . ' - ' . $this->end_date( $format );
	}

	/**
	 * Returns a date range for the start/end date of this event.
	 *
	 * @param bool|string $start False or Unix timestamp
	 * @param bool|string $end   False or Unix timestamp
	 *
	 * @return false|string
	 */
	function date_range( $start = false, $end = false ) {
		$start = $start ? $start : $this->start_date();
		$end   = $end ? $end : $this->end_date();

		// If on same month/day/year. (E.g. August 12, 2017)
		if ( date( 'mdY', $start ) === date( 'mdY', $end ) ) {
			return date( 'F j, Y', $start );
		}

		// If on same month/year. (E.g. August 12 - 30, 2017)
		if ( date( 'mY', $start ) === date( 'mY', $end ) ) {
			return date( 'F j', $start ) . ' - ' . date( 'j, Y', $end );
		}

		// If on same year. (E.g. August 12 - September 10, 2017)
		if ( date( 'Y', $start ) === date( 'Y', $end ) ) {
			return date( 'F j', $start ) . ' - ' . date( 'F j, Y', $end );
		}

		// Otherwise, return full date range. (E.g. August 12th, 2017 - September 10 2018)
		return date( 'F j, Y', $start ) . ' - ' . date( 'F j, Y', $end );
	}

	/**
	 * Copy of link() function from Timber/Post, plus adding the occurrence_id
	 * to the end so we can use that to get the date on the single event page.
	 *
	 * @return string
	 */
	function link() {
		if ( isset( $this->_permalink ) ) {
			return $this->_permalink;
		}
		$this->_permalink = get_permalink( $this->ID );

		return $this->_permalink . '?r=' . $this->occurrence_id;
	}

	/**
	 * Return the venue as a Timber\Post.
	 *
	 * @return \Timber\Term|bool
	 */
	function venue() {
		$venue = new Venue( eo_get_venue( $this->ID ) );

		if ( $venue->ID ) {
			return $venue;
		}

		return false;
	}

	/**
	 * Returns the venue address.
	 *
	 * @return \Timber\Term|bool
	 */
	function venue_address() {
		$venue = $this->venue();

		if ( $venue ) {
			return $venue->get_address();
		}

		return false;
	}

	/**
	 * Return edit link for front end editing.
	 *
	 * @return bool|string
	 */
	function edit_link() {
		if ( function_exists( 'eo_fes_event_edit_link' ) ) {
			return eo_fes_event_edit_link( $this->id ) . '?r=' . $this->occurrence_id;
		}

		return false;
	}
}
