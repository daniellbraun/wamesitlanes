<?php
/**
 * The template for displaying a single event
 *
 * Please note that since 1.7, this template is not used by default. You can edit the 'event details'
 * by using the event-meta-event-single.php template.
 *
 * Or you can edit the entire single event template by creating a single-event.php template
 * in your theme. You can use this template as a guide.
 *
 * For a list of available functions (outputting dates, venue details etc) see http://codex.wp-event-organiser.com/
 *
 ***************** NOTICE: *****************
 *  Do not make changes to this file. Any changes made to this file
 * will be overwritten if the plug-in is updated.
 *
 * To overwrite this template with your own, make a copy of it (with the same name)
 * in your theme directory. See http://docs.wp-event-organiser.com/theme-integration for more information
 *
 * WordPress will automatically prioritise the template in your theme directory.
 ***************** NOTICE: *****************
 *
 * @package Event Organiser (plug-in)
 * @since   1.0.0
 */

$context         = Timber\Timber::get_context();
$post            = Timber\Timber::get_post();
$context['post'] = $post;

$date_format = eo_get_event_datetime_format( $post->ID );

$context['recurs'] = eo_recurs( $post->ID );

// Gets the next event date. Returns flase if the event has ended.
$context['next'] = eo_get_next_occurrence( eo_get_event_datetime_format( $post->ID ), $post->ID );

// Is a recurring event, so get occurrences.
if ( eo_recurs( $post->ID ) ) {
	// Check if 'r' query var is set. 'r' is an occurrence_id.
	if ( get_query_var( 'r' ) ) {
		// Get the value which is the occurrence ID for the event to be shown.
		$occurrence_id = get_query_var( 'r' );
		$occurrence    = eo_get_the_occurrence( $post->ID, $occurrence_id );

		// If the occurrence ID is valid, set that the date as the next date.
		if ( $occurrence ) {
			$context['occurrence_id'] = $occurrence_id;

			// Set next occurrence to the date of the occurrence ID.
			$context['next'] = eo_format_event_occurrence( $post->ID, $occurrence_id );
		}
	}

	// Start/end of occurrence series range.
	$context['start'] = eo_get_schedule_start( $date_format, $post->ID );
	$context['last']  = eo_get_schedule_last( $date_format, $post->ID );

	// Get all upcoming events in this series.
	$context['event_series'] = Timber\Timber::get_posts( array(
		'post_type'         => 'event',
		'event_start_after' => 'today',
		'posts_per_page'    => - 1,
		'event_series'      => $post->ID,
		'group_events_by'   => 'occurrence',
	) );

	// With the ID 'eo-upcoming-dates', JS will hide all but the next 5 dates, with options to show more.
	// Enqueues EO script to show/hide dates.
	wp_enqueue_script( 'eo_front' );
}

// Get 4 latest upcoming events.
$context['upcoming_events'] = eo_get_events( array(
	'numberposts'       => 4,
	'event_start_after' => 'today',
	'showpastevents'    => true, // Will be deprecated, but set it to true to play it safe.
) );


/*
 * Using the WordPress loop here as a workaround to get front end editing from
 * Event Organiser to work. EO checks for 'in_the_loop()' before it shows the
 * edit form, so we need to actually use the loop to trick it.
 */
if ( have_posts() ) {
	Timber\Timber::render( array(
		'single-event.twig',
		'single.twig',
	), $context );
}
