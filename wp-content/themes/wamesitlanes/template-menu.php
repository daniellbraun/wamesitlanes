<?php
/**
 * The template for displaying the eat or drink menu page.
 *
 * @package WordPress
 */

/*
 Template Name: Menu Page
*/

$context         = Timber::get_context();
$post            = new TimberPost();
$context['post'] = $post;

$context['menu_groups'] = get_field('menu_layout',$post->ID);

Timber::render( array( 'template-menu.twig', 'page.twig' ), $context );
