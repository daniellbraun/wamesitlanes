<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">

	<!--	<input type="submit" class="search-submit" value="Search" />-->
    <label for="search-button">Search Button</label>
    <button id="search-button" type="submit" class="search-submit" value="Search" >
		<svg class="icon">
			<use xlink:href="#search"></use>
		</svg></button>

    <label for="search-field">Search</label>
	<input id="search-field" type="search" class="search-field" placeholder="Search" value="" name="s" title="Search for:" />


</form>