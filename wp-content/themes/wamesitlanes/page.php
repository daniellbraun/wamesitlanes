<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * To generate specific templates for your pages you can use:
 * /mytheme/views/page-mypage.twig
 * (which will still route through this PHP file)
 * OR
 * /mytheme/page-mypage.php
 * (in which case you'll want to duplicate this file and save to the above path)
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context         = Timber\Timber::get_context();
$post            = new Timber\Post();
$context['post'] = $post;

$templates = array(
	'page-' . $post->post_name . '.twig',
	'page.twig',
);

if ( is_front_page() ) {

	$context['specials'] = new Timber\PostQuery( array(
		'post_type' => 'specials',
		'posts_per_page' => -1,
		'order' => 'ASC',
		'orderby' => 'menu_order',
	) );

	array_unshift( $templates, 'front-page.twig' );

}

if ( is_page('attractions') ) {

	$context[ 'attraction_pages' ] = new Timber\PostQuery( array(
		'post_type'      => 'page',
		'posts_per_page' => -1,
		'post_parent'    => $post->ID,
		'order'          => 'ASC',
		'orderby'        => 'menu_order',
	) );



};

Timber\Timber::render( $templates, $context );
