<?php
/**
 * The template for displaying Event Category pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package     WordPress
 * @subpackage  Timber
 * @since       Timber 0.2
 */

$context          = Timber\Timber::get_context();
$context['post']  = new Timber\Post( get_field( 'event' . '_archive', 'option' ) ); // Get the corresponding archive page.
$context['posts'] = new Timber\PostQuery();

$event_category_args             = array(
	'taxonomy'   => 'event-category',
	'hide_empty' => false,
);

$term = new Timber\Term();
$context['term_slug'] = $term->slug;
$context['term_name'] = $term->name;
$context['term_desc'] = $term->description;

$context['event_categories'] = \Timber\Timber::get_terms( $event_category_args );

Timber\Timber::render( array(
	'taxonomy-event.twig',
	'taxonomy.twig',
	'index.twig',
), $context );
