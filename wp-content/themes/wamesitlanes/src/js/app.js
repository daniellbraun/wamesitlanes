(function ($) {
    $(document).foundation();

    $('.specials').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [

            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 640,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.quotes').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
    });

    const linked = document.querySelectorAll("[data-link]");

    if (linked.length) {
        linked.forEach( (el) => {
            el.style.cursor = "pointer";
            const link = el.dataset.link;
            el.addEventListener("click", () => {
                window.location = link;
            })
        })
    }
})(jQuery);
