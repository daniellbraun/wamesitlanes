/**
 * Created by danielbraun on 4/10/18.
 */
(function ($) {

    const $categoryButtons = $('.category');

    if ($categoryButtons.length) {

        const slug = document.querySelector('.event-categories a.active').dataset.category;

        if ( ! $('[data-term]').length) {
            // Replace the first page history, so we can load in our state.
            history.replaceState({type: 'event', slug}, null, window.location.href);
        }

        const $categoryButtonsChildren = $('.category *');

        const updateXs = () => {
            const active_category_box = document.querySelector('.event-categories a.active .check-box');
            const inactive_category_box = document.querySelectorAll('.event-categories a .check-box');

            $(inactive_category_box).html('');
            $(active_category_box).html('&times;');
        };

        const loadEventsData = (category, successCallback) => {

            $.ajax({
                url: vi.ajax_url,
                type: 'GET',
                data: {
                    security: vi.security,
                    action: 'events',
                    category: category,
                },
                dataType: 'json',
                success: successCallback,
                error: function (error) {
                    console.log('error :( ');
                }
            })
        };

        const loadEvents = (e) => {
            e.preventDefault();

            $categoryButtons.removeClass('active');
            $(e.currentTarget).addClass('active');
            updateXs();

            const slug = e.target.dataset.category;

            loadEventsData(slug, function (result) {

                document.querySelector('.events-container').innerHTML = result.content;
                document.querySelector('.event-heading').innerHTML = (result.category.name != '') ? result.category.name : '';
                document.querySelector('.event-content').innerHTML = (result.category.desc != '') ? '<p>' + result.category.desc + '</p>' : '';

                history.pushState({type: 'event', slug}, null, e.currentTarget.getAttribute('href'));
            })
        };

        /**
         * Event listeners
         */
        $categoryButtons.on('click', loadEvents);
        $categoryButtonsChildren.css('pointer-events', 'none');
        $categoryButtonsChildren.on('click', (e) => e.stopPropagation());

        window.addEventListener('popstate', function (e) {

            if (e.state && e.state.type === 'event') {

                loadEventsData(e.state.slug, function (result) {
                    document.querySelector('.events-container').innerHTML = result.content;
                    document.querySelector('.event-heading').innerHTML = (result.category.name != '') ? result.category.name : '';
                    document.querySelector('.event-content').innerHTML = (result.category.desc != '') ? '<p>' + result.category.desc + '</p>' : '';
                    $categoryButtons.removeClass('active');
                    $(`[data-category=${e.state.slug}]`).addClass('active');
                    updateXs();
                })
            }
        });

        /*
         * If paging is occurring, taxonomy-event.twig is loaded and the data attribute
         * "data-term" is located on the category menu container populated with the slug.
         * On page load, if this is detected, update the position of the "X" to the correct button
         */
        if ($('[data-term]').length) {
            $categoryButtons.removeClass('active');
            const paged_term_slug = document.querySelector('.event-categories').dataset.term;
            $(`[data-category=${paged_term_slug}]`).addClass('active');
            updateXs();

            //console.log(window.location.href);
            //console.log(paged_term_slug);

            //history.replaceState({type: 'event', paged_term_slug}, null, window.location.href);
        }
    }

})(jQuery);