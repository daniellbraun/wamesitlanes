<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package     WordPress
 * @subpackage  Timber
 * @since       Timber 0.2
 */

$context          = Timber\Timber::get_context();
$context['post']  = new Timber\Post( get_field( get_post_type() . '_archive', 'option' ) ); // Get the corresponding archive page.
$context['posts'] = new Timber\PostQuery();

if ( 'example-cpt' === get_post_type() ) {
	// Do stuff for example-cpt here.
}

if ( 'example-cpt2' === get_post_type() ) {
	// Do stuff for example-cpt2 here.
}

Timber\Timber::render( array(
	'archive-' . get_post_type() . '.twig',
	'archive.twig',
	'index.twig',
), $context );
